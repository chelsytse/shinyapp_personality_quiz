projpath <- file.path( getwd())


linear.transform <- function( x, xmin, xmax, ymin, ymax ) {
    xrng <- xmax - xmin
    yrng <- ymax - ymin
    y <- (x - xmin) * yrng / xrng + ymin
    return(y)
}

xcol.darkgrey <- rgb(120/255, 120/255, 120/255)
xcol.lightgrey <- rgb(230/255, 230/255, 230/255)
xcol.plotRegion <- rgb(120/255, 120/255, 120/255, 70/255)




############################### PLOT STUFF
############################### PLOT STUFF

labels <- c("E","A","C","ES","O")
labels.lower <- tolower(labels)

d <- length(labels)

xthetas <- seq( pi/2, -3*pi/2, length=d+1 )[ 1:(d) ] ; xthetas

rad.labs <- 1.5
smin.rad <- 0.1 
smax.rad <- rad.labs-0.15

#points on axis
rad.adjp <- seq(smin.rad, smax.rad, 0.02)
x.adjp <- as.vector(t(apply( matrix(rep(rad.adjp,d),nrow=length(rad.adjp),ncol=d), 1, function(x) x*cos(xthetas))))
y.adjp <- as.vector(t(apply( matrix(rep(rad.adjp,d),nrow=length(rad.adjp),ncol=d), 1, function(x) x*sin(xthetas))))
axisp <- data.frame('x'=x.adjp, 'y'=y.adjp)



xxpoly1 <- c(
(rad.labs+0.15) * cos( seq(0, 2*pi, length=140) ),
(rad.labs-0.15) * cos( seq(2*pi, 0, length=140) )
)
yypoly1 <- c(
(rad.labs+0.15) * sin( seq(0, 2*pi, length=140) ),
(rad.labs-0.15) * sin( seq(2*pi, 0, length=140) )
)


xxpoly.circ <- c(
( (smax.rad+smin.rad)*0.5 ) * cos( seq(0, 2*pi, length=70) )
)
yypoly.circ <- c(
( (smax.rad+smin.rad)*0.5) * sin( seq(0, 2*pi, length=70) )
)





############################### Submit Data Stuff
############################### Submit Data Stuff

## Saved table
savedtbl <- matrix(rep(numeric(0),11+d),nrow=0,ncol=11+d)
colnames(savedtbl) <- c(paste0("S",1:10), 'Gender', labels)
savedtbl<-as.data.frame(savedtbl)


responsesDir <- file.path(projpath, "responses")
epochTime <- function() {
  as.integer(Sys.time())
}
humanTime <- function() format(Sys.time(), "%Y%m%d-%H%M%OS")


############################### Load Data Stuff
############################### Load Data Stuff
outputDir <- "shiny_response"
loadData <- function() {
  # Read all the files into a list
  filesInfo <- drop_dir(outputDir)
  filePaths <- filesInfo$path
  data <- lapply(filePaths, drop_read_csv, stringsAsFactors = FALSE)
  # Concatenate all data together into one data.frame
  data <- dplyr::rbind_all(data)
  data
}